package ru.t1.artamonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>, IUserOwnedRepository<Project> {

    void clear();

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

}
