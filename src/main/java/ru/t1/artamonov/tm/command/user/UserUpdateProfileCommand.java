package ru.t1.artamonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "update-user-profile";

    @NotNull
    private static final String DESCRIPTION = "update profile of current user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("FIRST NAME: ");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.print("LAST NAME: ");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.print("MIDDLE NAME: ");
        @NotNull final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
