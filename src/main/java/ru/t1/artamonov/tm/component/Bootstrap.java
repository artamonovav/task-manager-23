package ru.t1.artamonov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.ICommandRepository;
import ru.t1.artamonov.tm.api.repository.IProjectRepository;
import ru.t1.artamonov.tm.api.repository.ITaskRepository;
import ru.t1.artamonov.tm.api.repository.IUserRepository;
import ru.t1.artamonov.tm.api.service.*;
import ru.t1.artamonov.tm.command.AbstractCommand;
import ru.t1.artamonov.tm.command.project.*;
import ru.t1.artamonov.tm.command.system.*;
import ru.t1.artamonov.tm.command.task.*;
import ru.t1.artamonov.tm.command.user.*;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.artamonov.tm.exception.system.CommandNotSupportedException;
import ru.t1.artamonov.tm.model.Project;
import ru.t1.artamonov.tm.model.User;
import ru.t1.artamonov.tm.repository.CommandRepository;
import ru.t1.artamonov.tm.repository.ProjectRepository;
import ru.t1.artamonov.tm.repository.TaskRepository;
import ru.t1.artamonov.tm.repository.UserRepository;
import ru.t1.artamonov.tm.service.*;
import ru.t1.artamonov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserViewProfileCommand());
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("test", "test", "test@test.ru");
        @NotNull final User user = userService.create("user", "user", "user@user.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(test.getId(), new Project("TEST PROJECT 1", "Test project", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("DEMO PROJECT 1", "Demo project", Status.NOT_STARTED));
        projectService.add(test.getId(), new Project("BEST PROJECT 1", "Best project", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("BETA PROJECT 1", "Beta project", Status.COMPLETED));

        taskService.create(test.getId(), "MEGA TASK 1");
        taskService.create(test.getId(), "BETA TASK 1");

        projectService.add(user.getId(), new Project("TEST PROJECT 2", "Test project", Status.IN_PROGRESS));
        projectService.add(user.getId(), new Project("DEMO PROJECT 2", "Demo project", Status.NOT_STARTED));
        projectService.add(user.getId(), new Project("BEST PROJECT 2", "Best project", Status.IN_PROGRESS));
        projectService.add(user.getId(), new Project("BETA PROJECT 2", "Beta project", Status.COMPLETED));

        taskService.create(user.getId(), "MEGA TASK 2");
        taskService.create(user.getId(), "BETA TASK 2");

    }

    private void processCommand(final String command) {
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.print("\nENTER COMMAND: ");
                @Nullable final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        @Nullable final String arg = args[0];
        try {
            processArgument(arg);
        } catch (@NotNull final Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

    private void exitApplication() {
        System.exit(0);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    public void run(@Nullable final String[] args) {
        if (processArguments(args)) exitApplication();
        initDemoData();
        initLogger();
        processCommands();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
